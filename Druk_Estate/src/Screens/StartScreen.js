import React from "react";
import { View , StyleSheet} from "react-native";
import Button from '../components/Button';
import Header from "../components/Header";
import Paragraph from "../components/Paragraph";
import Background from "../components/Background";

export default function StartScreen({navigation}){
    return(
        <Background>
        
            <Header style = {styles.header}>Druk Estate</Header>
            <Paragraph>Gets a place for you</Paragraph>
            <Button
            mode = 'outlined'
            onPress = {() => {
                navigation.navigate('LoginScreen')
            }}
            >Login</Button>
            <Button mode ='contained' onPress = {() => {
                navigation.navigate('RegisterScreen')
            }}>Sign UP</Button>
        </Background>
       
    )
}
const styles = StyleSheet.create({
    header:{
        fontFamily:'Roboto',
        fontWeight:'bold',
        fontSize:40,
        color:'blue'
    },
  
})
