import React, {useState} from 'react';
import {Text,View, StyleSheet, Touchable} from 'react-native'
import Button from '../components/Button';
import Header from '../components/Header';
import Paragraph from '../components/Paragraph';
import Background from '../components/Background';
import TextInput from '../components/TextInput';
// import { emailValidator } from '../core/helpers/emailValidator';
import { passwordValidator } from '../core/helpers/passwordValidator';
import { nameValidator } from '../core/helpers/nameValidator';
// import BackButton from '../BackButton';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { theme } from '../theme';

export default function RegisterScreen({navigation}){
    const [email, setEmail] = useState({value:'',error:''})
    const [password,setPassword] = useState({value:'', error:''})
    const [name,setName] = useState({value:"", error:""})

    const onSignUpPressed = () =>{
        const nameError = nameValidator(name.value);
        const emailError = emailValidator(email.value);
        const passwordError = passwordValidator(password.value);
        if(emailError || passwordError || nameError){
            setName({...name, error:nameError});
            setEmail({...email,error:emailError});
            setPassword({...password, error: passwordError});


        }
    }
    return(
        <Background>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
            <Header>Create Account</Header>
            <TextInput
            value={name.value}
            error={name.error}
            errorText = {name.error} 
            onChangeText={(text) => setName({value:text, error:''})}
            label='Name'
            />
            <TextInput
            label='Email'
            value={email.value}
            error = {email.error}
            errorText={email.error}
            onChangeText = {(text)=> setEmail({value:text, error:''})}
            />
            <TextInput
            value={password.value}
            error={password.error}
            errorText = {password.error} 
            onChangeText={(text)=> setPassword({value:text,error:''})}
            label = 'Password'
            secureTextEntry/>
            <Button mode='contained' onPress = {onSignUpPressed}>Sign Up</Button>
            <View style={styles.row}>
                <Text>Already have an account? </Text>
                <TouchableOpacity onPress={() => navigation.replace("LoginScreen")}>
                    <Text style={styles.link}>Login</Text>
                </TouchableOpacity>
            </View>
    
        </Background>
    )

}
const styles = StyleSheet.create({
    row:{
        flexDirection:'row',
        marginTop:4,
    },
    link:{
        fontWeight:'bold',
        color:theme.colors.primary,
    }
})


