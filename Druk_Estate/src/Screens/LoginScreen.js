import React, { useState } from "react";
import { Text, View, StyleSheet, TouchableOpacity } from "react-native";
import Button from "../components/Button";
import Header from "../components/Header";
import Paragraph from "../components/Paragraph";
import Background from "../components/Background";
import TextInput from "../components/TextInput";
import { passwordValidator } from "../core/helpers/passwordValidator";
import { BottomNavigation } from "react-native-paper";
import { theme } from "../theme";
import Backbutton from "../components/BackButton";

export default function LoginScreen({navigation}){
    const[PhoneNumber, setPhoneNumber] = useState({value:'', error:''})
    const[Password, setPassword] = useState({value:'', error:''})

    const onLoginPressed = () => {
        const phoneError = PhoneNumber.value ? '': "Phone number can't be empty."
        const passwordError = passwordValidator(Password.value);
        if (phoneError || passwordError){
            setPhoneNumber({...PhoneNumber, error:phoneError});
            setPassword({...Password, error:passwordError})
        }
    }
    return(
        <Background>
            <Backbutton goBack = {navigation.goBack}/>
            <Header style = {styles.header}>Login</Header>
            <TextInput 
            label = 'Phone Number'
            value= {PhoneNumber.value}
            error = {PhoneNumber.error}
            errorText = {PhoneNumber.error}
            onchangeText = {(text) => setPhoneNumber({value:text, error:''})}
            />
            <TextInput 
            
            label = 'Password'
            value = {Password.value}
            error = {Password.error}
            errorText = {Password.error}
            onchangeText = {(text) => setPassword({value:text, error:''})}
            secureTextEntry
            />
            <View style = {styles.forgotPassword}>
                <TouchableOpacity
                onPress={() => navigation.navigate('ResetPasswordScreen')}
                
                >
                    <Text style = {styles.forgot}>forgot your Password</Text>

                </TouchableOpacity>
            </View>
            <Button mode='contained' onPress = {onLoginPressed}>Login</Button>
            <View style = {styles.row}>
                <Text>Don't have an account?</Text>
                <TouchableOpacity onPress={() => BottomNavigation.replace("RegistrationScreen")}>
                    <Text style = {styles.link}>SignUp</Text>
                </TouchableOpacity>
            </View>
        </Background>
    )
}
const styles = StyleSheet.create({
    header:{
        fontFamily:'Roboto',
        fontWeight:'bold',
        fontSize:34
    },
    row:{
        flexDirection:'row',
        marginTop: 4,
    },
    link:{
        fontWeight:'bold',
        color:theme.colors.primary,
    }
})