import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-native-paper';
import { theme } from './src/theme';
import Button from './src/components/Button';
import TextInput from './src/components/TextInput';
import Header from './src/components/Header';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import { StartScreen , LoginScreen, RegisterScreen} from './src/Screens/index';
import 'react-native-gesture-handler';

const Stack = createStackNavigator();
export default function App() {
  return (
    <Provider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator
        initialRouteName = 'StartScreen'
        screenOptions = {{headerShown:false}}

        >
          <Stack.Screen name = 'StartScreen' component = {StartScreen}/>
          <Stack.Screen name='LoginScreen' component={LoginScreen}/>
          <Stack.Screen name='RegisterScreen' component={RegisterScreen}/>
        </Stack.Navigator>
      </NavigationContainer>
  
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
